<?
class wp_cms__admin__admin extends wp_cms__admin__admin__parent
{
	function load($d = null)
	{
		parent::{__function__}();
		$this->D['NAVIGATION']['D']['LEFT']['D']['CMS']['LANGUAGE']['D']['DE'] = [ 'AWESOME_ICON'	=> 'fa-newspaper-o', 'TITLE' => 'CMS Seiten'];
		$this->D['NAVIGATION']['D']['LEFT']['D']['CMS']['D']['CMS']['LANGUAGE']['D']['DE'] = array(
			'TITLE'			=> 'Alle Seiten',
			'LINK'			=> 'D[PAGE]=admin__cmspagelist',
			'AWESOME_ICON'	=> 'fa-list',
		);
		$this->D['NAVIGATION']['D']['LEFT']['D']['CMS']['D']['NEWCMS']['LANGUAGE']['D']['DE'] = array(
			'TITLE'			=> 'Neue Seite',
			'LINK'			=> 'D[PAGE]=admin__cmspage&D[ACTION]=new',
			'AWESOME_ICON'	=> 'fa-list',
		);
		$this->D['NAVIGATION']['D']['LEFT']['D']['CMS']['D']['MENU']['LANGUAGE']['D']['DE'] = array(
			'TITLE'			=> 'Navigation',
			'LINK'			=> 'D[PAGE]=admin__cmsmenu',
			'AWESOME_ICON'	=> 'fa-list',
		);
	}

}