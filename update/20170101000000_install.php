<?
class wp_cms__20170101000000_install
{
	function __construct(){ global $C, $D; $this->C = &$C; $this->D = &$D; }
	function __call($m, $a){ return $a[0]; }
	
	function up()
	{
		$this->C->db()->query("CREATE TABLE `cms_page` (
  `id` varchar(32) NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `language_id` varchar(32) NOT NULL,
  `itimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `utimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

		$this->C->db()->query("CREATE TABLE `cms_page_attribute` (
  `id` varchar(32) NOT NULL,
  `page_id` varchar(32) NOT NULL,
  `language_id` varchar(32) NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `text` text,
  `itimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `utimestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

		$this->C->db()->query("CREATE TABLE `cms_attribute` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `language_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `type` varchar(50) NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `itimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `utimestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");

		$this->C->db()->query("CREATE TABLE `cms_menu` (
  `id` varchar(32) NOT NULL,
  `parent_id` varchar(32) DEFAULT NULL,
  `language_id` varchar(32) NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `itimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `utimestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");


		$this->C->db()->query("ALTER TABLE `cms_page` ADD PRIMARY KEY (`id`,`language_id`) USING BTREE;");
		$this->C->db()->query("ALTER TABLE `cms_page_attribute` ADD PRIMARY KEY (`id`,`page_id`,`language_id`);");
		$this->C->db()->query("ALTER TABLE `cms_attribute` ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`,`language_id`);");
		$this->C->db()->query("ALTER TABLE `cms_menu` ADD PRIMARY KEY (`id`,`language_id`) USING BTREE, ADD KEY `parent_id` (`parent_id`), ADD KEY `language_id` (`language_id`);");

		return 1;
	}
	
	function down()
	{
		return 1;
	}
}