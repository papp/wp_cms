<?
class wp_cms__class__cms extends wp_cms__class__cms__parent
{
	function __construct(&$D = null)
	{
		parent::{__function__}($D);
    }

	function get_menu()
	{
		$D = &$this->D['MODUL']['D']['wp_cms'];
		
		$W = $this->C->db()->where_interpreter(array(
			'ID'					=> "id IN ('ID')",
			'PARENT_ID'				=> "parent_id IN ('PARENT_ID')",
			),$D['MENU']['W']);
		$qry = $this->C->db()->query("SELECT id, parent_id,language_id,active,url,title,utimestamp,itimestamp FROM cms_menu WHERE 1 {$W} ORDER BY parent_id, language_id, title");
		while ($ary = $qry->fetch_array(MYSQLI_ASSOC))
		{
				$D['MENU']['D'][ $ary['id'] ] = array (
					'PARENT_ID' 	=> $ary['parent_id'],
				);
				$D['MENU']['D'][ $ary['id'] ]['LANGUAGE']['D'][ $ary['language_id'] ] = array(
					'ACTIVE'		=> $ary['active'],
					'URL'			=> $ary['url'],
					'TITLE'			=> $ary['title'],
					'ITIMESTAMP'	=> $ary['itimestamp'],
					'UTIMESTAMP'	=> $ary['utimestamp'],
				);
				$D['MENU']['D'][ $ary['id'] ]['PARENT_ID'] = $ary['parent_id'];
				$D['MENU']['PARENT']['D'][ $ary['parent_id'] ]['CHILD']['D'][$ary['id']]['ACTIVE'] = $ary['active'];
		}
	}
	
	function set_menu()
	{
		$D = &$this->D['MODUL']['D']['wp_cms'];
		foreach((array)$D['MENU']['D'] as $k => $v )
		{
			foreach((array)$v['LANGUAGE']['D'] as $kl => $vl )
			{
				if($vl['ACTIVE'] != -1)
				{
					$IU_LAN .= (($IU_LAN)?',':'')."('{$k}','{$kl}'";
					$IU_LAN .= (isset($v['PARENT_ID']))?", '{$v['PARENT_ID']}'":", NULL";
					$IU_LAN .= (isset($vl['ACTIVE']))?", '{$vl['ACTIVE']}'":", NULL";
					$IU_LAN .= (isset($vl['URL']))?", '{$vl['URL']}'":", NULL";
					$IU_LAN .= (isset($vl['TITLE']))?", '{$vl['TITLE']}'":", NULL";
					$IU_LAN .= ")";
				}
				else
				{
					$DEL_LAN .= ($DEL_LAN?',':'')."'{$k}{$kl}'";
				}
			}
		}
		
		if($IU_LAN)
			$this->C->db()->query("INSERT INTO cms_menu (id,language_id,parent_id,active,url,title) VALUES {$IU_LAN}
									ON DUPLICATE KEY UPDATE
									parent_id = CASE WHEN VALUES(parent_id) IS NOT NULL THEN VALUES(parent_id) ELSE cms_menu.parent_id END,
									active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE cms_menu.active END,
									url = CASE WHEN VALUES(url) IS NOT NULL THEN VALUES(url) ELSE cms_menu.url END,
									title = CASE WHEN VALUES(title) IS NOT NULL THEN VALUES(title) ELSE cms_menu.title END
									");
									
		if($DEL_LAN)
		{
			$this->C->db()->query("DELETE FROM cms_menu WHERE id,language_id IN ({$DEL})");
		}
	}
	
	function get_template()
	{
		$D = &$this->D['MODUL']['D']['wp_cms'];
		$a = glob('tmp/system/frontend__cms__*');
		foreach($a AS $x)
		{
			$name = basename($x,'.php');
			if($name != "frontend__cms___")
				$D['TEMPLATE']['D'][$name]['TITLE'] = str_replace('frontend__cms__','',$name); 
		}
	}
	
	function get_page()
	{
		$D = &$this->D['MODUL']['D']['wp_cms'];
		
		$W = ($D['PAGE']['W']['ID'])?" AND id IN ('{$D['PAGE']['W']['ID']}')":'';
		
		$qry = $this->C->db()->query("SELECT id,active,utimestamp,itimestamp FROM cms_page WHERE 1 {$W} ");
		while ($ary = $qry->fetch_array(MYSQLI_ASSOC))
		{
				$D['PAGE']['D'][ $ary['id'] ] = array(
					'ACTIVE'		=> $ary['active'],
					'ITIMESTAMP'	=> $ary['itimestamp'],
					'UTIMESTAMP'	=> $ary['utimestamp'],
				);
		}
		
		$IDs = implode("','",array_keys((array)$D['PAGE']['D']));
		$qry = $this->C->db()->query("SELECT id,page_id,language_id,active,text,utimestamp,itimestamp
									FROM cms_page_attribute WHERE page_id IN ('{$IDs}')");
		while ($ary = $qry->fetch_array(MYSQLI_ASSOC))
		{
				$D['PAGE']['D'][ $ary['page_id'] ]['LANGUAGE']['D'][ $ary['language_id'] ]['ATTRIBUTE']['D'][ $ary['id'] ] = array(
					'ACTIVE'		=> $ary['active'],
					'VALUE'			=> $ary['text'],
					'ITIMESTAMP'	=> $ary['itimestamp'],
					'UTIMESTAMP'	=> $ary['utimestamp'],
				);
		}
	}

	function set_page()
	{
		$D = &$this->D['MODUL']['D']['wp_cms'];
		foreach((array)$D['PAGE']['D'] as $k => $v )
		{
			if($v['ACTIVE'] != -1)
			{
				$IU .= (($IU)?',':'')."('{$k}'";
				$IU .= (isset($v['ACTIVE']))?", '{$v['ACTIVE']}'":", NULL";
				$IU .= ")";
				
				foreach((array)$v['LANGUAGE']['D'] as $kl => $vl )
				{
					foreach((array)$vl['ATTRIBUTE']['D'] as $ka => $va )
					{
						if($vl['ACTIVE'] != -1)
						{
							$IU_ATT .= (($IU_ATT)?',':'')."('{$k}','{$kl}','{$ka}'";
							$IU_ATT .= (isset($va['ACTIVE']))?", '{$va['ACTIVE']}'":", NULL";
							$IU_ATT .= (isset($va['VALUE']))?", '{$va['VALUE']}'":", NULL";
							$IU_ATT .= ")";
						}
						else
						{
							$DEL_ATT .= ($DEL_ATT?',':'')."'{$k}{$kl}{$ka}'";
						}
					}
				}
			}
			else
			{
				$DEL .= ($DEL?',':'')."'{$k}'";
			}
		}
		
		if($IU)
			$this->C->db()->query("INSERT INTO cms_page (id,active) VALUES {$IU}
									ON DUPLICATE KEY UPDATE
									active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE cms_page.active END
									");
		
		if($IU_ATT)
			$this->C->db()->query("INSERT INTO cms_page_attribute (page_id,language_id,id,active,text) VALUES {$IU_ATT}
									ON DUPLICATE KEY UPDATE
									active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE cms_page_attribute.active END,
									text = CASE WHEN VALUES(text) IS NOT NULL THEN VALUES(text) ELSE cms_page_attribute.text END
									");
									
		if($DEL)
		{
			$this->C->db()->query("DELETE FROM cms_page WHERE id IN ({$DEL})");
			$this->C->db()->query("DELETE FROM cms_page_attribute WHERE page_id NOT IN (SELECT id FROM cms_page)");
		}
		if($DEL_ATT)
			$this->C->db()->query("DELETE FROM cms_page_attribute WHERE CONCAT(page_id,language_id,id) IN ({$DEL_ATT})");
	}
	
	function get_attribute()
	{
		$W = ($this->D['MODUL']['D']['wp_cms']['ATTRIBUTE']['W']['ID'])?" AND id IN ('{$this->D['MODUL']['D']['wp_cms']['ATTRIBUTE']['W']['ID']}')":'';
		
		$qry = $this->C->db()->query("SELECT id,language_id,active,type,title,utimestamp,itimestamp
									FROM cms_attribute WHERE 1 {$W} ");
		while ($ary = $qry->fetch_array(MYSQLI_ASSOC))
		{
				$this->D['MODUL']['D']['wp_cms']['LANGUAGE']['D'][ $ary['language_id'] ]['ATTRIBUTE']['D'][ $ary['id'] ] = array(
					'ACTIVE'		=> $ary['active'],
					'TYPE'			=> $ary['type'],
					'VALUE'			=> $ary['title'],
					'ITIMESTAMP'	=> $ary['itimestamp'],
					'UTIMESTAMP'	=> $ary['utimestamp'],
				);
		}
	}
	
	function set_attribute()
	{
		foreach($this->D['ATTRIBUTE']['D'] as $k => $v )
		{
			foreach($v[ $k ] as $k1 => $v1 )
			{
				if($v1['ACTIVE'] != -1)
				{
					$IU .= (($IU)?',':'')."('{$k}','{$k1}'";
					$IU .= (isset($v1['ACTIVE']))?", '{$v1['ACTIVE']}'":", NULL";
					$IU .= (isset($v1['TYPE']))?", '{$v1['TYPE']}'":", NULL";
					$IU .= (isset($v1['VALUE']))?", '{$v1['VALUE']}'":", NULL";
					$IU .= ")";
				}
				else
				{
					$DEL .= ($DEL?',':'')."'{$k}{$k1}'";
				}
			}
		}
		
		if($IU)
			$this->C->db()->query("INSERT INTO attribute (id,attribute_id,active,type,`value`) VALUES {$IU}
									ON DUPLICATE KEY UPDATE
									active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE seo_url.active END,
									seo_url_md5 = CASE WHEN VALUES(seo_url) IS NOT NULL THEN MD5(VALUES(seo_url)) ELSE seo_url.seo_url_md5 END,
									seo_url = CASE WHEN VALUES(seo_url) IS NOT NULL THEN VALUES(seo_url) ELSE seo_url.seo_url END,
									url = CASE WHEN VALUES(url) IS NOT NULL THEN VALUES(url) ELSE seo_url.url END
									");
		if($DEL)
			$this->C->db()->query("DELETE FROM attribute WHERE CONCAT(id,attribute_id) IN ({$DEL})");
	}
} 