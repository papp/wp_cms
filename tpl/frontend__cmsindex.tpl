{block name="meta" append}
	<meta name='title' content='{$D.MODUL.D['wp_cms'].PAGE.D[$D.MODUL.D['wp_cms'].PAGE.W.ID].LANGUAGE.D['DE'].ATTRIBUTE.D['TITLE'].VALUE}'/>
{/block}
{block name="content"}
<div class="container bs-docs-container">
	<div class="row">
		<div class="col-md-3" role="complementary">
			<nav class="bs-docs-sidebar hidden-print hidden-sm hidden-xs affix">
				<ul class="nav bs-docs-sidenav">
					<li class=""> <a href="#glyphicons">Glyphicons</a>
					<li class=""> <a href="#glyphicons">Glyphicons</a>
					<li class=""> <a href="#glyphicons">Glyphicons</a>
				</ul>
			</nav>
		</div>
		<div class="col-md-9" role="complementary">
			<div class="page-header">
				<h1>{$D.MODUL.D['wp_cms'].PAGE.D[$D.MODUL.D['wp_cms'].PAGE.W.ID].LANGUAGE.D['DE'].ATTRIBUTE.D['TITLE'].VALUE}</h1>
			</div>
			{$D.MODUL.D['wp_cms'].PAGE.D[$D.MODUL.D['wp_cms'].PAGE.W.ID].LANGUAGE.D['DE'].ATTRIBUTE.D['TEXT'].VALUE}
		</div>
	</div>
</div>
{/block}
