<form id="formPAGE" style="padding:5px 0;">
{$D.LANGUAGE.D =  ['DE' => 1]}
{foreach from=$D.MODUL.D['wp_cms'].PAGE.D name=PAGE item=PAGE key=kPAGE}
<ul class="nav nav-tabs" id="myTabs" role="tablist">
	{foreach from=$D.LANGUAGE.D name=LANG item=LANG key=kLANG}
	<li role="presentation" {if $kLANG == 'DE'}class="active"{/if}><a href="#lang{$kLANG}" id="lang{$kLANG}-tab" role="tab" data-toggle="tab" aria-controls="lang{$kLANG}" aria-expanded="true">{$kLANG}</a></li>
	{/foreach}
</ul>
<div class="tab-content" id="myTabContent" style="background:#fff;padding:5px;">
			
		{foreach from=$D.LANGUAGE.D name=LANG item=LANG key=kLANG}
		<div class="tab-pane fade in {if $kLANG == 'DE'}active{/if}" role="tabpanel" id="lang{$kLANG}" aria-labelledby="lang{$kLANG}-tab">
			<table class="table">
				<thead>
					<th>Title</th>
					<th>Value</th>
				</thead>
				<tbody>
						<tr>
							<td>Aktive</td>
							<td><input name="D[MODUL][D][wp_cms][PAGE][D][{$kPAGE}][LANGUAGE][D][{$kLANG}][ACTIVE]" type="checkbox" {if $PAGE.ACTIVE}checked{/if}></td>
						</tr>
						<tr>
							<td>URL</td>
							<td>
								<div class="input-group">
									<span class="input-group-addon">from</span>
									<input type="hidden" name="D[MODUL][D][wp_seo][SEO][D][{$kPAGE}][ACTIVE]" value="1">
									<input type="text" class="form-control" name="D[MODUL][D][wp_seo][SEO][D][{$kPAGE}][SEO_URL]" value="{if $D.MODUL.D['wp_seo'].SEO.D[{$kPAGE}].SEO_URL}{$D.MODUL.D['wp_seo'].SEO.D[{$kPAGE}].SEO_URL}{else}{$kLANG}/page_{$kPAGE}{/if}">
									<input name="D[MODUL][D][wp_seo][SEO][D][{$kPAGE}][URL]" type="hidden" class="form-control" value="D[PAGE]=frontend__cms__one_column&D[MODUL][D][wp_cms][PAGE][W][ID]={$kPAGE}">
									<span class="input-group-addon">to</span>
									<select class="form-control">
										{foreach from=$D.MODUL.D['wp_cms'].TEMPLATE.D name=TEMP item=TEMP key=kTEMP}
										<option value='{$kTEMP}'>{$TEMP.TITLE}</option>
										{/foreach}
									</select>
									<span class="input-group-addon"><a href="?D[PAGE]=frontend__cms__one_column&D[MODUL][D][wp_cms][PAGE][W][ID]={$kPAGE}" target='_blank'>?D[PAGE]=frontend__cms__one_column&D[MODUL][D][wp_cms][PAGE][W][ID]={$kPAGE}</a></span>
								</div>
							</td>
						</tr>
						<tr>
							<td>Title</td>
							<td>
								<input name="D[MODUL][D][wp_cms][PAGE][D][{$kPAGE}][LANGUAGE][D][{$kLANG}][ATTRIBUTE][D][TITLE][VALUE]" type="text" class="form-control" value="{$PAGE.LANGUAGE.D[$kLANG].ATTRIBUTE.D['TITLE'].VALUE}">
							</td>
						</tr>
						<tr>
							<td>Text</td>
							<td><textarea name="D[MODUL][D][wp_cms][PAGE][D][{$kPAGE}][LANGUAGE][D][{$kLANG}][ATTRIBUTE][D][TEXT][VALUE]" class="form-control">{$PAGE.LANGUAGE.D[$kLANG].ATTRIBUTE.D['TEXT'].VALUE}</textarea>
								
							</td>
						</tr>
						{foreach from=$D.MODUL.D['wp_cms'].LANGUAGE.D[$kLANG].ATTRIBUTE.D name=ATT item=ATT key=kATT}
						<tr>
							<td>{$ATT.VALUE}</td>
							<td>
								{if $D.MODUL.D['wp_cms'].LANGUAGE.D[$kLANG].ATTRIBUTE.D[$kATT].TYPE == 'text'}
									<input name="D[MODUL][D][wp_cms][PAGE][D][{$kPAGE}][LANGUAGE][D][{$kLANG}][ATTRIBUTE][D][{$kATT}][VALUE]" type="text" class="form-control" value="{$PAGE.LANGUAGE.D[$kLANG].ATTRIBUTE.D[$kATT].VALUE}">
								{elseif $D.MODUL.D['wp_cms'].LANGUAGE.D[$kLANG].ATTRIBUTE.D[$kATT].TYPE == 'textarea'}
									<textarea name="D[MODUL][D][wp_cms][PAGE][D][{$kPAGE}][LANGUAGE][D][{$kLANG}][ATTRIBUTE][D][{$kATT}][VALUE]" class="form-control">{$PAGE.LANGUAGE.D[$kLANG].ATTRIBUTE.D[$kATT].VALUE}</textarea>
								{/if}
							</td>
						</tr>
						{/foreach}					
					</tbody>
				</table>
			</div>
			{/foreach}
			
<div class="text-right">
<div class="btn-group">
				<button type="button" onclick="$.ajax({ type: 'POST', dataType : 'json', url : '?D[PAGE]=admin__cmspage&D[ACTION]=set_page', data : $('#formPAGE').serialize() /*, success: function(data) { $('#page').load('?D[PAGE]=seourl');}*/ });" class="btn btn-default">Speichern</button>
				<button type="button" onclick="$('#page').load('?D[PAGE]=admin__cmspagelist'); return false;" class="btn btn-default">Abbrechen</button>
			</div>
		</div>
</div>
{/foreach}
</form>