<form id="formCMS">

	<table class="table">
		<thead>
			<th></th>
			<th>Title</th>
			<th>UDate</th>
			<th>IDate</th>
			<th><!--<button onclick="$('#page').load('?D[PAGE]=cmspage&D[MODUL][D][wp_cms][PAGE][W][ID]={$kPAGE}&D[MODUL][D][wp_cms][PAGE][D][{$smarty.now}][ACTIVE]=1'); return false;" type="button">+</button>--></th>
		</thead>
		<tbody>
			{foreach from=$D.MODUL.D['wp_cms'].PAGE.D name=PAGE item=PAGE key=kPAGE}
			<tr>
				<td><input type="checkbox" {if $PAGE.ACTIVE}checked{/if}></td>
				<td>{$PAGE.LANGUAGE.D['DE'].ATTRIBUTE.D['TITLE'].VALUE}</td>
				<td>{$PAGE.UTIMESTAMP}</td>
				<td>{$PAGE.ITIMESTAMP}</td>
				<td>
					<div class="btn-group btn-group-sm">
						<button class="btn btn-default" type="button" onclick="$('#page').load('?D[PAGE]=admin__cmspage&D[MODUL][D][wp_cms][PAGE][W][ID]={$kPAGE}'); return false;"><i class="fa fa-pencil-square-o"></i></button>
						<span class="btn btn-default"><a href="{$D.MODUL.D['wp_seo'].SEO.D[{$kPAGE}].SEO_URL}" target='_blank'><i class="fa fa-external-link"></i></a></span>
					</div>
				</td>
			</tr>
			{/foreach}
		</tbody>
	</table>

</form>