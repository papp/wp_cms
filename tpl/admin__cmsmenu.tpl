{function name=add_menu}
		{foreach from=$D.MODUL.D['wp_cms'].MENU.PARENT.D[$kMENU].CHILD.D name=CHILD item=CHILD key=kCHILD}
		<li style="list-style:none;">
			<input type="hidden" class="form-control" name="D[MODUL][D][wp_cms][MENU][D][{$kCHILD}][PARENT_ID]" value="{$kMENU}">
		
			{$MENU = $D.MODUL.D['wp_cms'].MENU.D[$kCHILD]}
			{foreach from=$MENU.LANGUAGE.D name=LANG item=LANG key=kLANG}
			<div class="input-group input-group-sm">
				<div class="input-group-btn">
					<button type="button" onclick="$.ajax({ url: '?D[PAGE]=admin__cmsmenu&D[ACTION]=add_menu&D[kMENU]={$kCHILD}', async: false,success : function(text){ $('#menu{$kCHILD}').append(text);}}); return false;" class="btn btn-success btn-xs"><i class="fa fa-plus" aria-hidden="true"></i></button>
				</div>
				<span class="form-control">{$kCHILD}</span>
				{if $MENU.PARENT_ID != ''}
				<span class="input-group-addon">TITLE:</span>
				<input type="text" class="form-control" name="D[MODUL][D][wp_cms][MENU][D][{$kCHILD}][LANGUAGE][D][{$kLANG}][TITLE]" value="{$LANG.TITLE}">
				<span class="input-group-addon">Target:</span>
				<select class="form-control">
					<option value="_self">_self</option>
					<option value="_blank">_blank</option>
				</select>
				<span class="input-group-addon">URL:</span>
				
				<div class="input-group-btn">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">Action <span class="caret"></span></button>
					<ul class="dropdown-menu">
						<li>
							<select class="form-control">
								<option value="_self">_self</option>
								<option value="_blank">_blank</option>
							</select>
						</li>
						<li><a href="#">Another action</a></li>
						<li><a href="#">Something else here</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="#">Separated link</a></li>
					</ul>
				</div>
				<input type="text" class="form-control" name="D[MODUL][D][wp_cms][MENU][D][{$kCHILD}][LANGUAGE][D][{$kLANG}][URL]" value="{$LANG.URL}">
				{/if}
			</div>
			{/foreach}
			<ul id='menu{$kCHILD}' style="padding-left:30px;">
				{add_menu D=$D kMENU=$kCHILD}
			</ul>
		</li>
		{/foreach}
{/function}
{if $D.ACTION == 'add_menu'}
	{add_menu D=$D kMENU=$D.kMENU}
{else}
<form id="formPAGE">
	<div class="tab-content">
			
		<ul class="nav nav-tabs" id="myTabs" role="tablist">
			{foreach from=$D.MODUL.D['wp_cms'].MENU.PARENT.D[''].CHILD.D name=CHILD item=CHILD key=kCHILD}
			<li role="presentation" {if $smarty.foreach.CHILD.first}class="active"{/if}><a href="#{$kCHILD}" id="home-tab" data-toggle="tab">{$kCHILD}</a></li>
			{/foreach}
		</ul>
		{foreach from=$D.MODUL.D['wp_cms'].MENU.PARENT.D[''].CHILD.D name=CHILD item=CHILD key=kCHILD}
		<div class="tab-content panel-body tab-pane {if $smarty.foreach.CHILD.first}active{/if}" id="{$kCHILD}">
			<div class="panel-body">
				<ul style="padding-left:10px;">
				{add_menu D=$D kMENU=$kCHILD}
				</ul>
			</div>
		</div>
		{/foreach}
		<div class="panel-footer text-right">
			<div class="btn-group">
				<button type="button" onclick="$.ajax({ type: 'POST', dataType : 'json', url : '?D[PAGE]=admin__cmsmenu&D[ACTION]=set_menu', data : $('#formPAGE').serialize() /*, success: function(data) { $('#page').load('?D[PAGE]=seourl');}*/ });" class="btn btn-default">Speichern</button>
				<button type="button" onclick="$('#page').load('?D[PAGE]=admin__cmsmenu'); return false;" class="btn btn-default">Abbrechen</button>
			</div>
		</div>

	</div>
	
</form>

{/if}